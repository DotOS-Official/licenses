# DotOS Licenses

### Most of our repos follow Apache 2.0 and Few are GPL 2.0. 

**References:**
- [AOSP](https://source.android.com/setup/start/licenses)
- [Kernel.org](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/plain/COPYING)